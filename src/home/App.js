import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


import './App.css';
import Login from '../login/login'
class App extends Component {
  render() {
    return (
      <div className="App">
      <Router>
            <div>
               <ul>
                  <li><Link to={'/'}>Home</Link></li>
                  <li><Link to={'/login'}>Login</Link></li>
               </ul>
               <hr />
               
               <Switch>
                  <Route exact path='/login' component={Login} />
               </Switch>
            </div>
         </Router>
      </div>
    )
  }
}

export default App;
